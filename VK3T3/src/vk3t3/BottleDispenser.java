/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk3t3;

/**
 *BottleDispenser -luokka, kopioitu kurssin Java-oppaasta viikkotehtävien tekemiseksi
 * @author juuso
 */
public class BottleDispenser {
    
    private int bottles;
//    //The array for the Bottle-objects
//    private Bottle[] bottle_array;
    private int money;
    
    public BottleDispenser() {
        bottles = 5;
        money = 0;
//        //Initialize the array
//        bottle_array = new Bottle[bottles];
//        //Add Bottle-objects to the array
//        
//        for(int i = 0;i<bottles;i++) {
//            // Use the default constructor to create new Bottles
//            bottle_array[i] = new Bottle();
//        }
    }
    public void addMoney() {
        
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    public void buyBottle() {
        
        Bottle b = new Bottle(); //luodaan uusi pullo-olio
        
        if (money < b.price){
            System.out.println("Syötä rahaa ensin!");
        }
        else {
            if (bottles <= 0){
                System.out.println("Ei enää pulloja jäljellä.");
            }
            else {
                money -= 1;
                bottles -= 1;
                System.out.println("KACHUNK! " + b.name + " tipahti masiinasta!"); 
            }
        }
    }
    public void returnMoney() {
        
        money = 0;
        System.out.println("Klink klink. Sinne menivät rahat!");
    }
}