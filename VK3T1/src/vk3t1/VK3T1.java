/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk3t1;

/**
 *Viikon 3.tehtävä 1, tekijä: Juuso Siltala
 * @author juuso
 */
public class VK3T1 { //Vaihdetaan Mainclassiksi Vioessa
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BottleDispenser bd = new BottleDispenser(); //Luodaan bd-olio BottleDispenser luokasta
        bd.addMoney();
        bd.buyBottle();
        bd.buyBottle();
        bd.addMoney();
        bd.addMoney();
        bd.buyBottle();        
        bd.returnMoney();
    }
}    