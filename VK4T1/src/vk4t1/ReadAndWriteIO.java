/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk4t1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *Luokka, jolla voidaan lukea ja kirjoittaa tiedostoon, tekijä Juuso Siltala
 * opnro,0545306
 * @author juuso
 */
public class ReadAndWriteIO {
    
    public void ReadFile(String s) {
        
        try {
            BufferedReader br = new BufferedReader(new FileReader(s));
            String output = "";
            while((output = br.readLine()) != null){
                System.out.println(output);
            }
            br.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadAndWriteIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadAndWriteIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
