/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk3t2;

/**
 *Bottle luokka on luotu Java oppaan sivun 22 mallin mukaisesti
 * @author juuso
 */
public class Bottle {
    //Luokan Bottle muuttujat
    String name;
    String manufacturer;
    float total_energy;
    
    public Bottle() { //Rakentaja oletusarvoineen
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3f;
    }
    
}
