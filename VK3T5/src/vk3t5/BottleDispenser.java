/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk3t5;

import java.util.ArrayList;

/**
 *BottleDispenser -luokka, kopioitu kurssin Java-oppaasta viikkotehtävien tekemiseksi
 * @author juuso
 */
public class BottleDispenser {
    
    //ArrayList Bottle-olioille
    ArrayList<Bottle> bottles = new ArrayList();
    private float money = 0.00f;
    private int i = 1; //Valittu pullo
    
    public BottleDispenser() {
        
        money = 0;
         //Lisätään erilaisia pulloja listaan
        Bottle temp = new Bottle(i);//1.
        bottles.add(temp);
        
        i++;
        temp = new Bottle(i);//2.
        temp.size = 1.5f;
        temp.price = 2.2f;
        bottles.add(temp);
        
        i++;
        temp = new Bottle(i);//3.
        temp.manufacturer = "Coca-Cola";
        temp.name = "Coca-Cola Zero";
        temp.price = 2.0f;
        bottles.add(temp);
        
        i++;
        temp = new Bottle(i);//4.
        temp.manufacturer = "Coca-Cola";
        temp.name = "Coca-Cola Zero";
        temp.price = 2.5f;
        temp.size = 1.5f;
        bottles.add(temp);
        
        i++;
        temp = new Bottle(i);//5.
        temp.manufacturer = "Coca-Cola";
        temp.name = "Fanta Zero";
        temp.price = 1.95f;
        bottles.add(temp);
        
        i++;
        temp = new Bottle(i);//6.
        temp.manufacturer = "Coca-Cola";
        temp.name = "Fanta Zero";
        temp.price = 1.95f;
        bottles.add(temp);
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle(int option) {
        i = option - 1; //Koska indeksit alkavat 0:sta
        if(bottles.isEmpty()){
            System.out.println("Ei enää pulloja jäljellä.");
        }
        
        else {
            Bottle b = bottles.get(i); //Haetaan i olio ArrayListiltä
            if (money < b.price){
                System.out.println("Syötä rahaa ensin!");
            }
            
            else {
                money -= b.price;
                removeBottle();
                System.out.println("KACHUNK! " + b.name + " tipahti masiinasta!");
                resetIndexes();
            }
        }
    }
    
    public void returnMoney() {
        System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n", money);
         money = 0;
    }
    
    public void removeBottle() {
        bottles.remove(i);
    }
    
    public void printList() {
        if (bottles.isEmpty()) {
            System.out.println("Ei enää pulloja jäljellä.");
        }
        
        else {
            for(int j = 0; j < bottles.size(); j++) {
               Bottle b1 = bottles.get(j);
               System.out.println(b1.index + ". Nimi: " + b1.name + "\n" +
    "	Koko: " + b1.size +"	Hinta: " + b1.price);
            }
        }
    }
    
    public void resetIndexes() {
        for(i = 0; i < bottles.size(); i++) {
            Bottle temp = bottles.get(i);
            temp.index = i + 1; //Koska printattuna lista alkaa 1:stä
        }
    }
}
