/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk2t3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *2.viikon 3.tehtävä, tekijä Juuso Siltala
 * @author juuso
 */
public class VK2T3 { //Vaihdetaan Viopessa Mainclassiksi

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
            //Pääohjelma
            String name = ""; //Alustetaan input nimi ja quote
            String quote = "";
            System.out.print("Anna koiralle nimi: ");
            Scanner scan = new Scanner(System.in);
            name = scan.next();
            //Luodaan koira
            Dog eka = new Dog(name);//Uuden koiran nimi inputtina
            System.out.print("Mitä koira sanoo: "); //testataan huvin vuoksi kahta eri syötteentallennusta
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            try {
                quote = br.readLine();
            } catch (IOException ex) { //syötteen virheentarkistus
            }
            eka.speak(eka.name + ": " + quote);//Koiran speak -metodi tulostaa inputin
 
        }
}