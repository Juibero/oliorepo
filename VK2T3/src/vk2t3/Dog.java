/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk2t3;

/**
 *Koira luokka ottaa inputtina nimen ja tulostaa speakilla speakin inputin.
 * Tekijä Juuso Siltala
 * @author juuso
 */
public class Dog {
    String name;
    private String salanimi; // Viope vaatii jostakin syystä "private" sanan
    public Dog(String name){ //Rakentaja
        this.name = name;
        salanimi = name;
        
        System.out.println("Hei, nimeni on "+name+"!");
    }

    void speak(String quote){ //Puhumismetodi
    System.out.println(quote); //saadaan printattua koiran sanoma
    }
}

    

