/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk4t3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *Luokka, jolla voidaan lukea ja kirjoittaa tiedostoon, tekijä Juuso Siltala
 * opnro:0545306
 * @author juuso
 */
public class ReadAndWriteIO {
    
    public void ReadAndWrite(String in, String out) {
        
        try {
            BufferedReader br = new BufferedReader(new FileReader(in));
            BufferedWriter bw = new BufferedWriter(new FileWriter(out));
            String line = "";
            
            while((line = br.readLine()) != null) {
                //Katsotaan ettei rivi ole tyhjä ja alle 30 merkkiä pitkä
                if ((line.trim().isEmpty() != true) && (line.length() < 30)) {
                    bw.write(line+"\n");
                }
            }
            
            br.close();
            bw.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadAndWriteIO.class.getName()).log(Level.SEVERE, null, ex);
        
        } catch (IOException ex) {
            Logger.getLogger(ReadAndWriteIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
