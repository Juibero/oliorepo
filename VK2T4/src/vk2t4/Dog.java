/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk2t4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *Koira luokka ottaa inputtina nimen ja tulostaa speakilla speakin inputin.
 * Tekijä Juuso Siltala
 * @author juuso
 */
public class Dog {
    String name = "";
    String quote = "";
    private String salanimi; // Viope vaatii jostakin syystä "private" sanan
    public Dog(String name){//Rakentaja
        String trimmi = name.trim();
        if (trimmi.isEmpty()) {
            this.name = "Doge";
        } 
        else {
            this.name = trimmi;
        }
        salanimi = name;
        
        System.out.println("Hei, nimeni on "+this.name+"!");
    }
   // public Dog()
    void speak(String name,String quote){ //Puhumismetodi
        String trimmi = quote.trim();
        if (trimmi.isEmpty()){
            this.quote = "Much wow!";
            
        }
        else {
            this.quote = trimmi;
        }
    System.out.println(name+": "+this.quote); //saadaan printattua koiran sanoma
        if (this.quote == "Much wow!"){ //Ajetaan jos  sanoma on tyhjä
            System.out.print("Mitä koira sanoo: "); 
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            try {
                quote = br.readLine();
            } catch (IOException ex) { //syötteen virheentarkistus
            }
            speak(name,quote); //Kutsutaan metodia uudetaan, rekursio
        }
    }
}

