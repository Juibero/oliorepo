/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk3t4;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *Viikon 3.tehtävä 4, tekijä: Juuso Siltala
 * @author juuso
 */
public class VK3T4 { //Vaihdetaan Mainclassiksi Vioessa
    /**
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int option = 5; //Alustetaan 5:ksi, koska sitä ei käytetä valintarakenteessa
        BottleDispenser bd = new BottleDispenser(); //Luodaan bd-olio BottleDispenser luokasta
        while (true){ //While looppi pyörii, kunnes siitä breakataan ulos syöttämällä 0
            System.out.print("\n*** LIMSA-AUTOMAATTI ***\n" +
"1) Lisää rahaa koneeseen\n" +
"2) Osta pullo\n" +
"3) Ota rahat ulos\n" +
"4) Listaa koneessa olevat pullot\n" +
"0) Lopeta\n" +
"Valintasi: ");
            Scanner s = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
            
            if (s.hasNextInt()){
                option = s.nextInt(); //Tallennetaan inputin int option muuttujaan
            }
            
            switch (option){ //Luodaan valintarakenne switch-casen avulla
                case 1:
                    bd.addMoney();
                    break;
                case 2:
                    bd.buyBottle();
                    break;
                case 3:
                    bd.returnMoney();
                    break;
                case 4:
                    bd.printList();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Anna sopiva kokonaisluku");
            }
            option = 5; //Lopuksi alustetaan optio 5:ksi taas
        }
    }
}    