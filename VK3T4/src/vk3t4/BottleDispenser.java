/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk3t4;

import java.util.ArrayList;

/**
 *BottleDispenser -luokka, kopioitu kurssin Java-oppaasta viikkotehtävien tekemiseksi
 * @author juuso
 */
public class BottleDispenser {
    
    private int bottleqty;
    //ArrayList Bottle-olioille
    ArrayList<Bottle> bottles = new ArrayList();
    private int money;
    private int i; //On aina viimeisen pullon indeksi
    
    public BottleDispenser() {
        bottleqty = 6;
        money = 0;
        //Bottle-oliot ArrayListiin
        for(i = 1; i <= bottleqty; i++) {
            bottles.add(new Bottle(i));
        }
        i--; //Laitetaan i:lle sama arvo kuin listan viimeisellä pullolla
        i--; //Vähennetään kaksi kertaa, koska indeksita alkaa 0:sta
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle() {
        if(bottles.isEmpty()){
            System.out.println("Ei enää pulloja jäljellä.");
        }
        
        else {
            Bottle b = bottles.get(i); //Haetaan i olio ArrayListiltä
            if (money < b.price){
                System.out.println("Syötä rahaa ensin!");
            }
            
            else {
                money -= 1;
                bottleqty -= 1;
                removeBottle();
                System.out.println("KACHUNK! " + b.name + " tipahti masiinasta!");
            }
        }
    }
    
    public void returnMoney() {
        money = 0;
        System.out.println("Klink klink. Sinne menivät rahat!");
    }
    
    public void removeBottle() {
        bottles.remove(i);
        i--;
    }
    
    public void printList() {
        if (bottles.isEmpty()) {
            System.out.println("Ei enää pulloja jäljellä.");
        }
        
        else {
            for(int j = 0; j < bottles.size(); j++) {
               Bottle b1 = bottles.get(j);
               System.out.println(b1.index + ". Nimi: " + b1.name + "\n" +
    "	Koko: " + b1.size +"	Hinta: " + b1.price);
            }
        }
    }   
}