/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk2t5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *Koira luokka ottaa inputtina nimen ja tulostaa speakilla speakin inputin.
 * Tekijä Juuso Siltala
 * @author juuso
 */
public class Dog {
    String name = "";
    String quote = "";
    private String salanimi; // Viope vaatii jostakin syystä "private" sanan
    public Dog(String name){//Rakentaja
        String trimmi = name.trim();
        if (trimmi.isEmpty()) {
            this.name = "Doge";
        } 
        else {
            this.name = trimmi;
        }
        salanimi = name;
        
        System.out.println("Hei, nimeni on "+this.name);
    }

    void speak(String name,String quote){ //Puhumismetodi
        String trimmi = quote.trim();
        if (trimmi.isEmpty()){
            this.quote = "Much wow!";
            
        }
        else {
            this.quote = trimmi;
        }
        if (this.quote == "Much wow!"){ //Ajetaan jos sanoma on tyhjä
            System.out.print("Mitä koira sanoo: "); 
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            try {
                quote = br.readLine();
            } catch (IOException ex) { //syötteen virheentarkistus
            }
            speak(name,quote); //Kutsutaan metodia uudetaan, rekursio
        }
        Scanner scanner = new Scanner(trimmi); //Ajetaan trimmi skanneri metodiin
        while (scanner.hasNext()){ //Skannataan booleanit ja intit kunnes ei ole seuraavia
            if (scanner.hasNextBoolean()){
                System.out.println("Such boolean: "+ scanner.nextBoolean());
            }
            if (scanner.hasNextInt()){
                System.out.println("Such integer: "+ scanner.nextInt());
            }
            else{
                if (scanner.hasNext()){
                    System.out.println(scanner.next());
                }
            }
        }
        scanner.close(); //Suljetaan skanneri      
    }
}

