/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk4t5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;



/**
 *Luokka, jolla voidaan lukea ja kirjoittaa tiedostoon, tekijä Juuso Siltala
 * opnro:0545306
 * @author juuso
 */
public class ReadAndWriteIO {
    
    public void ReadAndWrite(String in, String out) {
        ZipFile zipFile; //Zip tiedoston alustus
        
        try {
            zipFile = new ZipFile(in); //Tiedoston nimi annetaan
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            
            while(entries.hasMoreElements()){ //Pyörii niin kauan kuin zipfilessä on asioita
                ZipEntry entry = entries.nextElement(); //Aina seuraava asia käsittelyyn
                InputStream stream = zipFile.getInputStream(entry);
                String line; //Luetaan tekstitiedosto rivi kerrallaan bufferedreaderilla
                BufferedReader br = new BufferedReader(new InputStreamReader(stream));
 
                while (((line = br.readLine()) != null)) { //Inputstreamista luetaan rivi kerrallaan
                    System.out.println(line);
                }
            }
        } catch (IOException ex) { //Virheenkäsittely
            Logger.getLogger(ReadAndWriteIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}