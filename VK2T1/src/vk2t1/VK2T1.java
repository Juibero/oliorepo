/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk2t1;

/**
 *1. toisen viikon viikkotehtävä. Tekijä Juuso Siltala
 * @author juuso
 */

public class VK2T1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
            //Pääohjelma
            
            Dog eka = new Dog("Rekku");//Uuden koiran nimi inputtina
            Dog toka = new Dog("Musti");
            eka.speak("Hau!");//Koiran speak -metodi tulostaa inputin
            toka.speak("Vuh!");

        }
}